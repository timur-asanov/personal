export default Object.freeze({
    ALL: "all", JAVA: "java", JS: "js", REACT: "react", ANGULAR: "angular", SPRING: "spring"
});
