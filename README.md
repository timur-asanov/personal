## Personal

[![pipeline status](https://gitlab.com/timur-asanov/personal/badges/master/pipeline.svg)](https://gitlab.com/timur-asanov/personal/-/commits/master) [![coverage report](https://gitlab.com/timur-asanov/personal/badges/master/coverage.svg)](https://gitlab.com/timur-asanov/personal/-/commits/master)

A repository for my personal website.

See it in action: https://timurasanov.com