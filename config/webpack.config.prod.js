const autoprefixer = require('autoprefixer');
const webpack = require('webpack');
const cssnano = require('cssnano');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');
const ModuleScopePlugin = require('react-dev-utils/ModuleScopePlugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const paths = require('./paths');

module.exports = {
    mode: 'production',
    bail: true,
    devtool: 'source-map',
    entry: paths.appIndexJs,
    output: {
        path: paths.appBuild,
        filename: 'static/js/[name].[chunkhash:8].js',
        chunkFilename: 'static/js/[name].[chunkhash:8].chunk.js',
        publicPath: '/'
    },
    resolve: {
        extensions: ['.js', '.json'],
        plugins: [
            new ModuleScopePlugin(paths.appSrc, [paths.appPackageJson]),
        ]
    },
    module: {
        strictExportPresence: true,
        rules: [
            {
                oneOf: [
                    {
                        test: /\.(js)$/,
                        include: paths.appSrc,
                        loader: require.resolve('babel-loader'),
                        options: { compact: true }
                    },
                    {
                        test: /\.module\.css$/,
                        use: [
                            {
                                loader: MiniCssExtractPlugin.loader,
                                options: {
                                    hmr: process.env.NODE_ENV === 'development'
                                }
                            },
                            {
                                loader: require.resolve('css-loader'),
                                options: {
                                    importLoaders: 1,
                                    sourceMap: true,
                                    modules: true,
                                }
                            },
                            {
                                loader: require.resolve('postcss-loader'),
                                options: {
                                    ident: 'postcss',
                                    plugins: () => [
                                        require('postcss-flexbugs-fixes'),
                                        autoprefixer({
                                            overrideBrowserslist: [
                                                '>1%',
                                                'last 4 versions',
                                                'Firefox ESR',
                                                'not ie < 9'
                                            ],
                                            flexbox: 'no-2009'
                                        }),
                                        cssnano({ preset: 'default' })
                                    ]
                                }
                            }
                        ],
                    },
                    {
                        test: /\.css$/,
                        use: [
                            {
                                loader: MiniCssExtractPlugin.loader,
                                options: {
                                    hmr: process.env.NODE_ENV === 'development'
                                }
                            },
                            {
                                loader: require.resolve('css-loader'),
                                options: {
                                    importLoaders: 1,
                                    sourceMap: true
                                }
                            },
                            {
                                loader: require.resolve('postcss-loader'),
                                options: {
                                    ident: 'postcss',
                                    plugins: () => [
                                        require('postcss-flexbugs-fixes'),
                                        autoprefixer({
                                            overrideBrowserslist: [
                                                '>1%',
                                                'last 4 versions',
                                                'Firefox ESR',
                                                'not ie < 9'
                                            ],
                                            flexbox: 'no-2009'
                                        }),
                                        cssnano({ preset: 'default' })
                                    ]
                                }
                            }
                        ],
                    },
                    {
                        test: /\.(ttf|eot|svg|woff2?)(.gz)?$/i,
                        loader: 'file-loader?name=static/fonts/[name].[ext]'
                    },
                    {
                        loader: require.resolve('file-loader'),
                        exclude: [/\.(js)$/, /\.html$/, /\.json$/],
                        options: {
                            name: '/static/media/[name].[hash:8].[ext]'
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            inject: true,
            template: paths.appHtml,
            minify: {
                removeComments: true,
                collapseWhitespace: true,
                removeRedundantAttributes: true,
                useShortDoctype: true,
                removeEmptyAttributes: true,
                removeStyleLinkTypeAttributes: true,
                keepClosingSlash: true,
                minifyJS: true,
                minifyCSS: true,
                minifyURLs: true
            },
        }),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('production')
        }),
        new MiniCssExtractPlugin({
            filename: 'static/css/[name].[hash:8].css',
            chunkFilename: '[id].css',
        }),
        new ManifestPlugin({
            fileName: 'asset-manifest.json'
        }),
        new UglifyJSPlugin({
            sourceMap: true
        }),
        new CompressionPlugin()
    ]
};
